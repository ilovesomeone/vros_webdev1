<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Events\SendMessage as SendMessage;
class LogresController extends Controller
{
    use AuthenticatesUsers;
    // protected $redirectTo = '/dashboard';

   public function Register(Request $request)
   {
        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users|email',
            'password' => 'required',
            'same_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }
        $password = bcrypt($request->password);

        DB::table('users')->insert([
            'email' => $request->email,
            'password' => $password,
            'name' => explode('@',$request->email)[0]
        ]);
        return redirect('/blade/login');
   }
   public function login(Request $request)
   {
    $data = DB::table('users')->where('email',$request->email)->first();
    if ($data) {
        $this->validateLogin($request);
        if ($this->hasTooManyLoginAttempts($request)) {
         $this->fireLockoutEvent($request);
         return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            //  dd(Auth::user());
            return redirect()->route('home.blade');
        }
        $this->incrementLoginAttempts($request);
        return redirect()->back()->withErrors(['messages' => 'wrong email or password']);
    }else{
        return redirect()->back()->withErrors(['messages' => 'Invalid Email Address']);
    }
   }
   public function logout()
   {
        Auth::logout();
   }
   public function Sidebar()
   {
      $get = DB::table('users')->select('name','id')->get();
      $chat = DB::table('chat')->join('users','users.id','chat.id_user')->get();
      
        //   dd($get);
      return view('chatting',compact('get','chat'));
   }

   public function chat_post(Request $request)
   {
    DB::table('chat')->insert([
        'id_user' => Auth::user()->id,
        'messages' => $request->messages
    ]);
    event(new SendMessage());

    return response()->json(['user' => Auth::user()->name,'messages' => $request->messages]);
   }

   public function logout_user(){
    Auth::logout();
    return redirect('/blade/login');
   }
}
