<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReactController extends Controller
{
    public function react(Request $request, $path='login')
    {
        // $path = 'login';
        // if($request->path()){
        //     $path = $request->path();
        // }
        return view('react', compact('path'));
    }
}
