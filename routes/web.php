<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/blade/login', 'bladeController@login')->name('login.blade');
Route::get('/blade/register', 'bladeController@register')->name('register.blade');
Route::post('/post_register', 'LogresController@Register');
Route::post('/post_login', 'LogresController@login');
Route::post('/blade/logout', 'LogresController@logout_user')->name('logout.blade');
Route::middleware(['auth'])->group(function () {
    Route::get('/blade/home', 'LogresController@Sidebar')->name('home.blade');  
    Route::post('chat_post','LogresController@chat_post');
});

Route::get('test',function ()
{
    event(new App\Events\SendMessage());

});
// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/react/{path?}', 'ReactController@react');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
