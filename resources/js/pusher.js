import Echo from "laravel-echo"

window.Echo = new Echo({
  broadcaster: 'pusher',
  key: '1d6f0db7a8ed3052d32e',
  cluster: 'ap1',
  forceTLS: true
});

var channel = window.Echo.channel('user-chat');
channel.listen('.user-chat', function(data) {
  alert(JSON.stringify(data));
});