

import React from 'react';

const withLayout = (Page)=>(props)=>{
return(<>
 <div className="bdy">
<img className="glb-ats" src="/vendor/assets/glb-ats.png" />
<img className="logo-lgn" src="/vendor/assets/logo.png" />
<img className="glb-bw" src="/vendor/assets/glb-bw.png" /> 
    {
        <Page {...{...props}} />
    }

<p className="footer">Powered by birutekno inc.</p>

</div>
</>)
}

export default withLayout