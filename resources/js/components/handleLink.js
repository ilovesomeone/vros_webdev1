const handleLink = (e, setState) =>{
    const page = e.target.dataset.id
    window.history.pushState(page.toUpperCase(), `${page.toUpperCase()}`, `/react/${page}`);
    setState(page)
}

export default handleLink