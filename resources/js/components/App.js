import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import Main from './Main';

function App({path}) {
    const [p, setPath] = useState(path)
    useEffect(()=>{
        console.log('as');
        
    }, [])
    return (

                <Main path={p} setPath={setPath} />
 
    );
}

export default App;

const path = document.getElementById('init-path').dataset.path
ReactDOM.render(<App path={path}/>, document.getElementById('app'));
document.getElementById('init-path').parentNode.removeChild(document.getElementById('init-path'))

