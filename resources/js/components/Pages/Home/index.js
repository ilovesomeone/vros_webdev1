import React from 'react';




function Home() {

    return (<div className="bdy">
        
        <div className="top-menu">
            <b className="b-top">Vros Chating</b>
            <a className="a-top" id="myBtn" onclick="myfunction()" href="#">Logout</a>
        </div>
        <hr className="top" />

        <div id="myModal" className="modal">
            <div className="modal-content">
                <p className="p-th">Are you sure to logout from this app?</p>
                <hr className="hr-modal" />
                <a className="a-c" id="myBtn" href="#">Cancel</a>
                <a className="a-y" id="myBtn" href="#">Yes</a>
            </div>

        </div>

        <div className="body-chat">
            <div className="card">

                <div className="container">
                    <h4><b className="b-partisipant">All Partisipant</b></h4>
                    <hr className="hr-partisipant" />
                    <div>
                        <table className="t-parti">
                            <tr>
                                <td><img className="partisipant-profile" src="/vendor/assets/user-logo-active.png"/><b
                                        className="b-parti-active">Jill</b></td>
                            </tr>
                            <tr>
                                <td><img className="partisipant-profile" src="/vendor/assets/user-logo-nonactive.png"/><b
                                        className="b-parti">Jill</b></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div className="card-pos">
                <div className="container">
                    <h4><b className="b-partisipant">General Room</b></h4>
                    <hr className="hr-partisipant-g" />
                    <div>
                        <table className="t-parti">
                            <tbody id="scroll-tbody">
                                <tr className="t-par-g">
                                    <td><img className="partisipant-profile-g" src="/vendor/assets/user-logo-active.png"/><b
                                            className="b-parti-active-g">Jill</b></td>
                                </tr>
                                <tr>
                                    <td className="msg-r"><b>Helloworld!!!</b></td>
                                </tr>
                                <tr className="t-par-g">
                                    <td><img className="partisipant-profile-g" src="/vendor/assets/user-logo-nonactive.png"/><b
                                            className="b-parti-g">Jill</b></td>
                                </tr>
                                <tr>
                                    <td className="msg-r"><b>Helloworld goooo!!!</b></td>
                                </tr>

                            </tbody>

                        </table>
                    </div>
                    <div className="g-b">
                        <hr className="hr-partisipant-g" />
                        {/* <form> */}
                            <input className="ch" type="text" id="chat" name="chat" placeholder="Type a message"/>
                            <input className="smt" type="submit" id="send" name="send" value="Send"/>
                        {/* </form> */}
                    </div>
                </div>
            </div>
        </div>

        <div className="side-bar">
            <img className="photo-profile" src="/vendor/assets/photo-profile.png"/>
            <hr className="side" />
            <div className="lg-bg">
                <img className="logo-msg" src="/vendor/assets/msg-logo.png"/>
            </div>
        </div>
    </div>);
}

export default Home;
