import React from 'react';
import handleLink from '../handleLink';
import withLayout from '../withLayout';

function Register({setPath}) {

    return (
        <div className="rgs-form">
            <p className="sign-in">Sign Up</p>
            <label>Email</label>
            <div>
                <input type="text" id="email" name="email" />
            </div>
            <label>Password</label>
            <div>
                <input type="password" id="password" name="password" />
            </div>
            <label>Confirmation Password</label>
            <div>
                <input type="password" id="password" name="password" />
            </div>
            <input type="submit" value="Sign Up" />
            <a className="register" onClick={(e)=>handleLink(e, setPath)} data-id="login">Already have an account?</a>
        </div>
    );
}

export default withLayout(Register);
