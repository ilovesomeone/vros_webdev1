import React from 'react'
import Login from "./Login";
import Register from "./Register";
import Home from "./Home";

const Pages = ({setPath, path})=>({
    login: <Login {...{setPath}}/>,
    register: <Register {...{setPath}}/>,
    home: <Home {...{setPath}}/>
})[path]

export default Pages