import React from 'react';
import handleLink from '../handleLink';
import withLayout from '../withLayout';




function Login({setPath}) {
    return (
        <div className="lgn-form">
            <p className="sign-in">Sign In</p>
            <label>Email</label>
            <div>
                <input type="text" id="email" name="email" />
            </div>
            <label>Password</label>
            <div>
                <input type="password" id="password" name="password" />
            </div>
            <input type="submit" value="Sign in" />
            <a className="register" onClick={(e)=>handleLink(e, setPath)} data-id="register">Don't have an account?</a>
        </div>
    );
}

export default withLayout(Login);
