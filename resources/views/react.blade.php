<!DOCTYPE html>
    <html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta id="init-path" data-path="{{ $path }}">
        <title>VROS - Virtual Office Solutions</title>

        <!-- FONT -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Quicksand">
        <!-- css -->
        <link rel="stylesheet" type="text/css" href="{{asset('vendor/css/style.css')}}">
    </head>
    <body>
        <div id="app"></div>

        <script src="{{ asset('js/app.js') }}"></script>
        <!-- <script src="{{ asset('vendor/js/app.js') }}"></script> -->

    </body>
    </html>