<!DOCTYPE html>
<html>

<head>
<meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>VROS - Virtual Office Solutions</title>

    <!-- FONT -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Quicksand">
    <!-- css -->
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/css/style.css')}}">
</head>

<body>
    <div class="bdy">
        <!-- top menu -->
        <div class="top-menu">
            <b class="b-top">Vros Chating</b>
            <a class="a-top" id="myBtn" onclick="myfunction()" href="#">Logout</a>
        </div>
        <hr class="top">

        <!-- Modal -->
        <div id="myModal" class="modal">
            <div class="modal-content">
                <p class="p-th">Are you sure to logout from this app?</p>
                <hr class="hr-modal">
                <a class="a-c" id="myBtn" href="#">Cancel</a>
                <a class="a-y logout-user" id="myBtn" href="#">Yes</a>
            </div>

        </div>

        <!-- body chat -->
        <div class="body-chat">
            <div class="card">
                <!-- list member -->
                <div class="container">
                    <h4><b class="b-partisipant">All Partisipant</b></h4>
                    <hr class="hr-partisipant">
                    <div>
                        <table class="t-parti" >
                            @foreach($get as $value)
                            @if($value->id == Auth::user()->id)
                            <tr>
                                <td><img class="partisipant-profile" src="{{asset('vendor/assets/user-logo-active.png')}}"><b
                                        class="b-parti-active">{{$value->name}}</b></td>
                            </tr>
                            @else
                           
                            <tr>
                                <td><img class="partisipant-profile" src="{{asset('vendor/assets/user-logo-nonactive.png')}}"><b
                                        class="b-parti">{{$value->name}}</b></td>
                            </tr> 
                            @endif
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>

            <!-- chats -->
            <div class="card-pos">
                <!-- list member -->
                <div class="container">
                    <h4><b class="b-partisipant">General Room</b></h4>
                    <hr class="hr-partisipant-g">
                    <div>
                        <table class="t-parti" id="mytable">
                            <tbody id="scroll-tbody" >
                                @foreach($chat as $val)
                                <tr class="t-par-g">
                                    <td><img class="partisipant-profile-g" src="{{asset('vendor/assets/user-logo-active.png')}}"><b
                                            class="b-parti-active-g">{{$val->name}}</b></td>
                                </tr>
                                <tr>
                                    <td class="msg-r"><b>{{$val->messages}}</b></td>
                                </tr>
                                @endforeach

                            </tbody>

                        </table>
                    </div>
                    <div class="g-b">
                        <hr class="hr-partisipant-g">
                        <div>
                            <input class="ch" type="text" id="chat" name="chat" placeholder="Type a message">
                            <input class="smt" type="submit" id="send" name="send" value="Send">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- side bar -->
        <div class="side-bar">
            <img class="photo-profile" src="{{asset('vendor/assets/photo-profile.png')}}">
            <hr class="side">
            <div class="lg-bg">
                <img class="logo-msg" src="{{asset('vendor/assets/msg-logo.png')}}">
            </div>
        </div>
    </div>
    <!-- javascript -->
    <script src="//js.pusher.com/3.1/pusher.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{asset('vendor/js/script.js')}}"></script>
    <script src="{{ url('/js/pusher.js') }}" type="text/javascript"></script>

</body>

</html>