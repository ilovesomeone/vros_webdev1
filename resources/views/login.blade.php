<!DOCTYPE html>
<html>
<head>
    <title>VROS - Virtual Office Solutions</title>

    <!-- FONT -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Quicksand">
    <!-- css -->
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/css/style.css')}}">
    <!-- javascript -->
    <script src="{{asset('vendor/js/script.js')}}"></script>
</head>
<body>
    <div class="bdy">
        <!-- logo gelombang atas -->
        <img class="glb-ats" src="{{asset('vendor/assets/glb-ats.png')}}">
        <!-- LOGO VROS -->
        <img class="logo-lgn" src="{{asset('vendor/assets/logo.png')}}">
        <!-- logo gelombang bawah -->
        <img class="glb-bw" src="{{asset('vendor/assets/glb-bw.png')}}">

        <!-- login form -->
        <div class="lgn-form">
            <p class="sign-in">Sign In</p>
            <form method="post" action="{{url('post_login')}}">
            @csrf
                <label>Email</label>
                <div>
                    <input type="text" id="email" name="email">
                </div>
                <label>Password</label>
                <div>
                    <input type="password" id="password" name="password">
                </div>
                <input type="submit" value="Sign in">
                <a class="register" href="{{route('register.blade')}}">Don't have an account?</a>
            </form>
        </div>

        <p class="footer">Powered by birutekno inc.</p>

    </div>
</body>
</html>